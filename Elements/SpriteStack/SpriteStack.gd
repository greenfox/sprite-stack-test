tool
extends Sprite

class_name SpriteStack

export var spritestack:Texture setget setSpriteTexture
export var slice_count:int setget setSpriteLayers
export var camera_angle:float setget setCameraAngle

func _ready():
	pass

func setSpriteTexture(newTexture):
	spritestack = newTexture;
	get_material().set_shader_param("spritesheet", spritestack)

func setSpriteLayers(newCount):
	slice_count=newCount
	get_material().set_shader_param("slice_count", slice_count)

func setCameraAngle(newCameraAng):
	camera_angle = newCameraAng
	get_material().set_shader_param("camera_ang", camera_angle)
